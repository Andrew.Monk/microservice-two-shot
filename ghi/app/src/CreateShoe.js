import { useState } from 'react';

function CreateShoe() {
  const [name, setName] = useState('');
  const [style, setStyle] = useState('');
  const [manufacturer, setManufacturer] = useState('');
  const [size, setSize] = useState('');
  const [color, setColor] = useState('');
  const [picture, setPicture] = useState('');

  const handleNameChange = (event) => {
    setName(event.target.value);
  };

  const handleStyleChange = (event) => {
    setStyle(event.target.value);
  };

  const handleManufacturerChange = (event) => {
    setManufacturer(event.target.value);
  };

  const handleSizeChange = (event) => {
    setSize(event.target.value);
  };

    const handlePictureChange = (event) => {
    setPicture(event.target.value);
  };

    const handleColorChange = (event) => {
    setColor(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h3>Add to the collection!</h3>
          <form onSubmit={handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
              <input
                value={name}
                onChange={handleNameChange}
                placeholder="Name"
                name="name"
                required
                type="text"
                id="name"
                className="form-control"
              />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={style}
                onChange={handleStyleChange}
                placeholder="Style"
                name="Style"
                required
                type="Text"
                id="style"
                className="form-control"
              />
              <label htmlFor="style">Style</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={manufacturer}
                onChange={handleManufacturerChange}
                placeholder="manufacturer"
                name="manufacturer"
                required
                type="text"
                id="manufacturer"
                className="form-control"
              />
              <label htmlFor="manufacturer">Brand</label>
            <div className="form-floating mb-3">
              <input
                value={size}
                onChange={handleSizeChange}
                placeholder="size"
                name="size"
                required
                type="text"
                id="size"
                className="form-control"
              />
              <label htmlFor="manufacturer">Size</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={color}
                onChange={handleColorChange}
                placeholder="color"
                name="color"
                required
                type="text"
                id="color"
                className="form-control"
              />
              <label htmlFor="manufacturer">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={picture}
                onChange={handlePictureChange}
                placeholder="picture"
                name="picture"
                required
                type="picture"
                id="picture"
                className="form-control"
              />
              <label htmlFor="manufacturer">Picture</label>
            </div>
            {/* <div className="mb-3">
              <select
                value={size}
                onChange={handleSizeChange}
                required
                name="size"
                id="size"
                className="form-select"
              >
                <option value="">Size</option>
                {shoes.map((shoe) => {
                  return (
                    <option key={shoe.size} value={shoe.size}>
                      {shoe.size}
                    </option>
                  );
                })} */}
              {/* </select> */}
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

function ShoeFormCreate() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h2 className="display-5">Dont be lame!</h2>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4"></p>
      </div>
      <CreateShoe />
    </div>
  );
}

export default ShoeFormCreate;
