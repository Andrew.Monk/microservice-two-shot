import React, {useEffect, useState} from "react"

function HatCreateForm () {
    const [styleName, setStyleName] = useState('')
    const [fabric, setFabric] = useState('')
    const [color, setColor] = useState('')
    const [pictureUrl, setPictureUrl] = useState('')
    const [location, setLocation] = useState('')
    const [locations, setLocations] = useState([])

    const fetchData = async () => {
        const url = "http://localhost:8100/api/locations/"

        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setLocations(data.locations)
            console.log(data)
            console.log('test')
        }
    }

		const handleStyleNameChange = (event) => {
			const value = event.target.value
			setStyleName(value)
		}

		const handleFabricChange = (event) => {
			const value = event.target.value
			setFabric(value)
		}

		const handleColorChange = (event) => {
			const value = event.target.value
			setColor(value)
		}

		const handlePictureUrlChange = (event) => {
			const value = event.target.value
			setPictureUrl(value)
		}

		const handleLocationChange = (event) => {
			const value = event.target.value
			setLocation(value)
		}

		const handleSubmit = async (event) => {
			event.preventDefault()



		const data = {}

		data.style_name = styleName
		data.fabric = fabric
		data.color = color
		data.picture_url = pictureUrl
		data.location = location

		console.log(data)

		const hatUrl = "http://localhost:8090/api/hats/"
		const fetchConfig = {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'applications/json'
			}
		}


		const response = await fetch(hatUrl, fetchConfig)
		if (response.ok) {
			const newHat = await response.json()
			console.log(newHat)

				setFabric('')
				setColor('')
				setPictureUrl('')
				setStyleName('')

		}
	}



    useEffect(() => {
        fetchData()
    }, [])


    return (
			<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Create a new Hat</h1>
					<form onSubmit={handleSubmit} id="create-hat-form">
						<div className="form-floating mb-3">
							<input value={styleName} onChange={handleStyleNameChange} placeholder="Style Name" name='Nstyleame' required type="text" id="style_name" className="form-control"/>
							<label htmlFor="name">Style Name</label>
						</div>
						<div className="form-floating mb-3">
							<input value={fabric} onChange={handleFabricChange} placeholder="Room count" name='room_count' required type="text" id="fabric" className="form-control"/>
							<label htmlFor="room_count">Fabric</label>
						</div>
						<div className="form-floating mb-3">
							<input value={color} onChange={handleColorChange} placeholder="Color" name = 'color' required type="text" id="color" className="form-control"/>
							<label htmlFor="city">Color</label>
					 </div>
					 <div className="form-floating mb-3">
							<input value={pictureUrl} onChange={handlePictureUrlChange} placeholder="Picture Url" name = 'picture_url' required type="text" id="pictureUrl" className="form-control"/>
							<label htmlFor="city">Picture Url</label>
					 </div>
						<div className="mb-3">
							<select value={location} onChange={handleLocationChange} required name="location" id='location' className="form-select">
								<option value="">Choose a location</option>
								{locations.map(location => {
									return (
										<option key={location.closet_name} value={location.href}>
											{location.closet_name}
										</option>
									)
								})}
							</select>
						</div>
						<button className="btn btn-primary">Create</button>
					</form>
				</div>
			</div>
			</div>
    )
}

export default HatCreateForm
