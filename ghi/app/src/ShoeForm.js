import React, {useEffect, useState } from 'react';

function ShoeForm () {

  const [shoes, setShoes] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/shoes/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data)
      setShoes(data.shoes);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

return (
    <div className="px-4 py-5 my-5 text-center">
        <h2 className="display-5 fw-bold">Peep the kicks!</h2>
        <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
                Dont steal my croots, they're limited edition.
            </p>
        </div>
        {shoes.length > 0 ? (
            <table className='table table-striped ma-auto'>
                <thead>
                    <tr key='info'>
                    <th key='name'>Name</th>
                    <th key='style'>Style</th>
                    <th key='manufacturer'>Manufacturer</th>
                    <th key='size'>Size</th>
                    <th key='color'>Color</th>
                    <th key='image'>Image</th>
                    </tr>
                </thead>
                <tbody>
                    {shoes.map(shoe => (
                        <tr key={shoe.id}>
                            <td key='name'>{shoe.name}</td>
                            <td key='style'>{shoe.style}</td>
                            <td key='brand'>{shoe.manufacturer}</td>
                            <td key='size'>{shoe.size}</td>
                            <td key='color'>{shoe.color}</td>
                            <td><img width='100' src={shoe.picture_url} /></td>
                        </tr>
                    ))}
                </tbody>
            </table>
        ) : (
            <p>Loading data...</p>
        )}
    </div>
)
}
export default ShoeForm
