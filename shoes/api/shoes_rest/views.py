# from django.shortcuts import render
from .models import Shoes, BinVO
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "import_href",
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "name",
        'style',
        "manufacturer",
        "bin",
        "color",
        'picture_url',
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }


class ShoeDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "name",
        "style",
        "size",
        "manufacturer",
        "color",
        "picture_url",
        "bin",
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    content = Shoes.objects.all()
    if request.method == "GET":
        print(content)
        return JsonResponse(
            {"shoes": content},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            print(content)
            bin_href = content["bin"]
            print(bin_href)
            bin = BinVO.objects.get(import_href=bin_href)
            print(bin)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoes = Shoes.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(['DELETE', 'GET', 'PUT'])
def api_shoe_detail(request, id):
    if request.method == 'GET':
        shoes = Shoes.objects.get(id=id)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False,

        )
    elif request.method == 'DELETE':
        count, _ = Shoes.objects.filter(id=id).delete()
        return JsonResponse(
            {'deleted': count > 0}
            )

    else:
        content = json.loads(request.body)
        try:
            if 'bin' in content:
                bin = Shoes.objects.get(bin=content['closet_name'])
                content['bin'] = bin

        except Shoes.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid bin id'}, status=400,
            )
        Shoes.objects.filter(id=id).update(**content)

        shoes = Shoes.objects.get(id=id)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False
        )
